package com.example.ucok.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ucok.Adapter.RecycleInformasi;
import com.example.ucok.R;
import com.example.ucok.Retorfit.ClientBuilder;
import com.example.ucok.Retorfit.Informasi.ResponseInformasiCovid;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment2 extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView daftar;
    private String mParam1;
    private String mParam2;

    public Fragment2() {

    }

    public static Fragment2 newInstance(String param1, String param2) {
        Fragment2 fragment = new Fragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_2, container, false);
        daftar = root.findViewById(R.id.recycle);
        tampilkanDaftar();
        return root;
    }
    void tampilkanDaftar(){
        Call<ResponseInformasiCovid> call = ClientBuilder.getInstasiasi().buatAPI().ambilSemuaInformasi();
        call.enqueue(new Callback<ResponseInformasiCovid>() {
            @Override
            public void onResponse(Call<ResponseInformasiCovid> call, Response<ResponseInformasiCovid> response) {
                System.out.println(response.body().getStatus());
                System.out.println(response.body().getInformasikasuscovid().size());
                daftar.setLayoutManager(new LinearLayoutManager(getContext()));
                RecycleInformasi listHeroAdapter = new RecycleInformasi(response.body().getInformasikasuscovid());
                daftar.setAdapter(listHeroAdapter);
            }

            @Override
            public void onFailure(Call<ResponseInformasiCovid> call, Throwable t) {

            }
        });
    }
}