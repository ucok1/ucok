package com.example.ucok.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.example.ucok.R;
import com.example.ucok.Retorfit.ClientBuilder;
import com.example.ucok.Retorfit.Informasi.ResponseCOVID;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment1 extends Fragment {
    private static final String ARG_TITLE = "title";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Fragment1() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static Fragment1 newInstance(String param1, String param2) {
        Fragment1 fragment = new Fragment1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_1, container, false);
        final AnyChartView anyChartView = root.findViewById(R.id.any_chart_view);
        Call<ResponseCOVID> call = ClientBuilder.getInstasiasi().buatAPI().ambilInformasi();

        call.enqueue(new Callback<ResponseCOVID>() {
            @Override
            public void onResponse(Call<ResponseCOVID> call, Response<ResponseCOVID> response) {
                Cartesian cartesian = AnyChart.column();
                List<DataEntry> data = new ArrayList<>();
                data.add(new ValueDataEntry("Positif", response.body().getPositif()));
                data.add(new ValueDataEntry("Sembuh", response.body().getSembuh()));
                data.add(new ValueDataEntry("Meninggal", response.body().getMeningal()));
                Column column = cartesian.column(data);

                column.tooltip()
                        .titleFormat("{%X}")
                        .position(Position.CENTER_BOTTOM)
                        .anchor(Anchor.CENTER_BOTTOM)
                        .offsetX(0d)
                        .offsetY(5d)
                        .format("{%Value}{groupsSeparator: }");

                cartesian.animation(true);
                cartesian.title("Grafik Nilai Anda");

                cartesian.yScale().minimum(0d);

                cartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }");

                cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
                cartesian.interactivity().hoverMode(HoverMode.BY_X);

                cartesian.xAxis(0).title("Kategori");
                cartesian.yAxis(0).title("Angka");

                anyChartView.setChart(cartesian);
            }

            @Override
            public void onFailure(Call<ResponseCOVID> call, Throwable t) {
                Toast.makeText(getContext(),"Tidak dapat tersambung  karena " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        return root;
    }

    public static Fragment1 getInstance(String title) {
        Fragment1 fra = new Fragment1();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        fra.setArguments(bundle);
        return fra;
    }
}