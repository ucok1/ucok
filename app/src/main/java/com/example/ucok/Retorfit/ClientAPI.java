package com.example.ucok.Retorfit;

import com.example.ucok.Retorfit.Informasi.ResponseCOVID;
import com.example.ucok.Retorfit.Informasi.ResponseInformasiCovid;
import com.example.ucok.Retorfit.Panduan.ResponsePanduan;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ClientAPI {
    @GET("informasi")
    Call<ResponseCOVID> ambilInformasi();

    @GET("informasiSemua")
    Call<ResponseInformasiCovid> ambilSemuaInformasi();
    @GET("dampak")
    Call<ResponsePanduan> ambilDampak();
}
