package com.example.ucok.Retorfit.Panduan;

import java.util.List;

public class ResponsePanduan {
    String status;
    List<Panduan> data;

    public ResponsePanduan(String status, List<Panduan> paduan) {
        this.status = status;
        this.data = paduan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Panduan> getPaduan() {
        return data;
    }

    public void setPaduan(List<Panduan> paduan) {
        this.data = paduan;
    }
}
