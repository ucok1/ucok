package com.example.ucok.Retorfit.Informasi;

public class InformasiCovid {
    int id;
    int positif;
    int sembuh;
    int meningal;
    String tanggal_buat;

    public InformasiCovid(int id, int positif, int sembuh, int meninggal, String tanggal_buat) {
        this.id = id;
        this.positif = positif;
        this.sembuh = sembuh;
        this.meningal = meninggal;
        this.tanggal_buat = tanggal_buat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPositif() {
        return positif;
    }

    public void setPositif(int positif) {
        this.positif = positif;
    }

    public int getSembuh() {
        return sembuh;
    }

    public void setSembuh(int sembuh) {
        this.sembuh = sembuh;
    }

    public int getMeninggal() {
        return meningal;
    }

    public void setMeninggal(int meninggal) {
        this.meningal = meninggal;
    }

    public String getTanggal_buat() {
        return tanggal_buat;
    }

    public void setTanggal_buat(String tanggal_buat) {
        this.tanggal_buat = tanggal_buat;
    }
}
