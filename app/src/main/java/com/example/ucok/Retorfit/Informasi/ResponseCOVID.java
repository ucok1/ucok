package com.example.ucok.Retorfit.Informasi;

public class ResponseCOVID {
    int positif;
    int sembuh;
    int meningal;
    int total;

    public ResponseCOVID(int positif, int sembuh, int meningal, int total) {
        this.positif = positif;
        this.sembuh = sembuh;
        this.meningal = meningal;
        this.total = total;
    }

    public int getPositif() {
        return positif;
    }

    public void setPositif(int positif) {
        this.positif = positif;
    }

    public int getSembuh() {
        return sembuh;
    }

    public void setSembuh(int sembuh) {
        this.sembuh = sembuh;
    }

    public int getMeningal() {
        return meningal;
    }

    public void setMeningal(int meningal) {
        this.meningal = meningal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
