package com.example.ucok.Retorfit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientBuilder {
    private static  final  String baseURL = "http://192.168.43.161/UcokWebService/public/";
    private  static ClientBuilder intasiasi;
    private Retrofit retrofit;
    private  ClientBuilder(){
        retrofit = new Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build();
    }
    public  static  synchronized  ClientBuilder getInstasiasi(){
        if(intasiasi == null){
            intasiasi = new ClientBuilder();
        }
        return intasiasi;
    }
    public ClientAPI buatAPI(){
        return retrofit.create(ClientAPI.class);
    }
}
