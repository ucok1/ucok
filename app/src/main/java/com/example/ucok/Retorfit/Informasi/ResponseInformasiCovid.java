package com.example.ucok.Retorfit.Informasi;

import java.util.List;

public class ResponseInformasiCovid {
        String status;
        List<InformasiCovid> informasikasuscovid;

        public ResponseInformasiCovid(String status, List<InformasiCovid> informasikasuscovid) {
                this.status = status;
                this.informasikasuscovid = informasikasuscovid;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public List<InformasiCovid> getInformasikasuscovid() {
                return informasikasuscovid;
        }

        public void setInformasikasuscovid(List<InformasiCovid> informasikasuscovid) {
                this.informasikasuscovid = informasikasuscovid;
        }
}
