package com.example.ucok.ui.main;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.ucok.InformasiKasus;
import com.example.ucok.R;
import com.example.ucok.Retorfit.ClientBuilder;
import com.example.ucok.Retorfit.Informasi.ResponseCOVID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceholderFragment extends Fragment {
    TextView sembuh,positif,meninggal,total;
    private static final String ARG_SECTION_NUMBER = "section_number";

//    private PageViewModel pageViewModel;

    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
//        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_informasi_covid, container, false);
        Call<ResponseCOVID> call = ClientBuilder.getInstasiasi().buatAPI().ambilInformasi();
        sembuh = root.findViewById(R.id.sembuh);
        positif = root.findViewById(R.id.positif);
        meninggal = root.findViewById(R.id.meninggal);
        total = root.findViewById(R.id.total);
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Menunggu Respon Dari Server");
        progressDoalog.setTitle("proses pemuatan");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDoalog.show();
        call.enqueue(new Callback<ResponseCOVID>() {
            @Override
            public void onResponse(Call<ResponseCOVID> call, Response<ResponseCOVID> response) {
                progressDoalog.dismiss();
                startCountAnimation(response.body().getPositif(),response.body().getSembuh(),response.body().getMeningal(),response.body().getTotal());
            }

            @Override
            public void onFailure(Call<ResponseCOVID> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getContext(),"Tidak dapat tersambung  karena " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

//        final TextView textView = root.findViewById(R.id.section_label);
//        pageViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        return root;
    }
    private void startCountAnimation(int number1,int number2,int number3,int number4) {
        ValueAnimator angka = ValueAnimator.ofInt(0, number1);
        ValueAnimator angka2 = ValueAnimator.ofInt(0, number2);
        ValueAnimator angka3 = ValueAnimator.ofInt(0, number3);
        ValueAnimator angka4 = ValueAnimator.ofInt(0, number4);
        angka.setDuration(1000);
        angka.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                positif.setText(animation.getAnimatedValue().toString());
            }
        });
        angka2.setDuration(1000);
        angka2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                sembuh.setText(animation.getAnimatedValue().toString());
            }
        });
        angka3.setDuration(1000);
        angka3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                meninggal.setText(animation.getAnimatedValue().toString());
            }
        });
        angka4.setDuration(1000);
        angka4.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                total.setText(animation.getAnimatedValue().toString());
            }
        });
        angka.start();
        angka2.start();
        angka3.start();
        angka4.start();
    }

}