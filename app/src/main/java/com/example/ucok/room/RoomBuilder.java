package com.example.ucok.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities =  {Repository.class},version = 1,exportSchema = false)
public abstract class RoomBuilder extends RoomDatabase {
    public abstract RepositoryDAO simpanContent();
}
