package com.example.ucok.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "repository")

public class Repository {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "judul")
    public  String judul;
    @ColumnInfo(name = "isi")
    public  String isi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
