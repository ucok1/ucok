package com.example.ucok.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;
@Dao
public interface RepositoryDAO {
    @Query("SELECT * FROM Repository")
    List<Repository> showContent();
    @Insert()
    long insert(Repository content);
}
