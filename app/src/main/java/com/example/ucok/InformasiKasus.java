package com.example.ucok;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ucok.Retorfit.ClientBuilder;
import com.example.ucok.Retorfit.Informasi.InformasiCovid;
import com.example.ucok.Retorfit.Informasi.ResponseCOVID;
import com.example.ucok.Retorfit.Informasi.ResponseInformasiCovid;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InformasiKasus extends AppCompatActivity {
    TextView sembuh,positif,meninggal,total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_kasus);
        Call<ResponseCOVID> call = ClientBuilder.getInstasiasi().buatAPI().ambilInformasi();
        sembuh = findViewById(R.id.sembuh);
        positif = findViewById(R.id.positif);
        meninggal = findViewById(R.id.meninggal);
        total = findViewById(R.id.total);
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(InformasiKasus.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Menunggu Respon Dari Server");
        progressDoalog.setTitle("proses pemuatan");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDoalog.show();
        call.enqueue(new Callback<ResponseCOVID>() {
            @Override
            public void onResponse(Call<ResponseCOVID> call, Response<ResponseCOVID> response) {
                progressDoalog.dismiss();
                startCountAnimation(response.body().getPositif(),response.body().getSembuh(),response.body().getMeningal(),response.body().getTotal());
            }

            @Override
            public void onFailure(Call<ResponseCOVID> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(InformasiKasus.this,"Tidak dapat tersambung  karena " + t.getMessage(), 50000).show();
            }
        });
    }
    private void startCountAnimation(int number1,int number2,int number3,int number4) {
        ValueAnimator angka = ValueAnimator.ofInt(0, number1);
        ValueAnimator angka2 = ValueAnimator.ofInt(0, number2);
        ValueAnimator angka3 = ValueAnimator.ofInt(0, number3);
        ValueAnimator angka4 = ValueAnimator.ofInt(0, number4);
        angka.setDuration(1000);
        angka.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                positif.setText(animation.getAnimatedValue().toString());
            }
        });
        angka2.setDuration(1000);
        angka2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                sembuh.setText(animation.getAnimatedValue().toString());
            }
        });
        angka3.setDuration(1000);
        angka3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                meninggal.setText(animation.getAnimatedValue().toString());
            }
        });
        angka4.setDuration(1000);
        angka4.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                total.setText(animation.getAnimatedValue().toString());
            }
        });
        angka.start();
        angka2.start();
        angka3.start();
        angka4.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.about){
            startActivity(new Intent(getApplicationContext(),About.class));
            finish();

        }
        return super.onOptionsItemSelected(item);
    }
}