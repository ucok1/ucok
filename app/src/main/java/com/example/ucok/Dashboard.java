package com.example.ucok;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Dashboard extends AppCompatActivity {
    CardView informasiKasus;
    CardView rumahSakitRujukan;
    CardView caraPenyebaran;
    CardView gejala;
    CardView penyebab;
    CardView pencegahan;
    CardView handSanitizer;
    CardView latarBelakang;
    CardView dampak;
    CardView penanganan;
    CardView about;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        informasiKasus = (CardView) findViewById(R.id.informasiKasus);

        informasiKasus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, InformasiCovid.class);
                startActivity(intent);
            }
        });

        rumahSakitRujukan = (CardView) findViewById(R.id.rumahSakitRujukan);

        rumahSakitRujukan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, RumahSakitRujukan.class);
                startActivity(intent);
            }
        });

        caraPenyebaran = (CardView) findViewById(R.id.caraPenyebaran);

        caraPenyebaran.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, CaraPenyebaran.class);
                startActivity(intent);
            }
        });

        gejala = (CardView) findViewById(R.id.gejala);

        gejala.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, Gejala.class);
                startActivity(intent);
            }
        });

        penyebab = (CardView) findViewById(R.id.penyebab);

        penyebab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, PenyebabTerinfeksi.class);
                startActivity(intent);
            }
        });

        pencegahan = (CardView) findViewById(R.id.pencegahan);

        pencegahan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, Pencegahan.class);
                startActivity(intent);
            }
        });

        handSanitizer = (CardView) findViewById(R.id.handSanitizer);

        handSanitizer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, Sanitizer.class);
                startActivity(intent);
            }
        });

        latarBelakang = (CardView) findViewById(R.id.latarBelakang);

        latarBelakang.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, LatarBelakang.class);
                startActivity(intent);
            }
        });

        dampak = (CardView) findViewById(R.id.dampak);

        dampak.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, DampakCovid.class);
                startActivity(intent);
            }
        });

        penanganan = (CardView) findViewById(R.id.penanganan);

        penanganan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, PenangananCovid.class);
                startActivity(intent);
            }
        });
        about = (CardView) findViewById(R.id.about);

        about.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, About.class);
                startActivity(intent);
            }
        });
    }
}
