package com.example.ucok.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ucok.R;
import com.example.ucok.Retorfit.Informasi.InformasiCovid;

import java.util.List;

public class RecycleInformasi extends  RecyclerView.Adapter<RecycleInformasi.Holder> {
    private List<InformasiCovid> data;
    public RecycleInformasi(List<InformasiCovid> data){
        this.data = data;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.kasus,parent,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.tanggal.setText(data.get(position).getTanggal_buat());
        holder.positif.setText("Positif : " + data.get(position).getPositif());
        holder.sembuh.setText(" Sembuh : " + data.get(position).getSembuh());
        holder.meningal.setText(" Meninggal : " + data.get(position).getMeninggal());
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tanggal;
        TextView positif;
        TextView sembuh;
        TextView meningal;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tanggal = itemView.findViewById(R.id.tanggal);
            positif = itemView.findViewById(R.id.positif);
            sembuh = itemView.findViewById(R.id.sembuh);
            meningal = itemView.findViewById(R.id.meninggal);
        }
    }
}
